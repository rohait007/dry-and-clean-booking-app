## Dry & Clean Service Booking Application
Application provide web portal. User can create customer, and book services for customer. 

## Config
Composer Insatll or Update " composer update "
Migrate with seeder " php artisan migrate --seed "

## Development 
Web portal build on Laravel & Livewire.
For datatable we use Livewire PowerGrid Package.

## Migration 
Migrate application with seeder "php artisan migrate --seed"

## Test User 
Email : user@booking.com ||
Password : password
