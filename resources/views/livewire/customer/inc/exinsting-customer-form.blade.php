        <div class="row mb-5">
            <div class="col-12 col-md-4 mx-auto">
                <label for="" class="form-label">Search</label>
                <input wire:model.debounce.200ms='searchEmail' class="form-control mx-auto shadow-top" type="text" placeholder="Email" onclick="this.select()">

                @if (count($customers) > 0)
                    <div class="card mt-1">
                        @foreach ($customers as $row)
                            @if ($row != null)
                                <a class="pl-2 py-2 pointer border-bottom dataList" wire:click='selectUser({{ $row->id }})'>
                                    <div><strong>{{ $row->first_name ?? 'No Name' }}</strong></div>
                                    <div>
                                        <small>{{ $row->email }} </small>
                                    </div>

                                </a>
                            @endif
                        @endforeach
                    </div>
                @endif
            </div>
        </div>
        @if ($content)
            <form wire:submit.prevent='save' class="row g-3 needs-validation" novalidate>

                <div class="col-md-6">
                    <label for="first_name" class="form-label">First Name</label>
                    <input wire:model.defer='input.first_name' type="text" class="form-control" id="first_name" placeholder="First Name">
                    @error('input.first_name')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="last_name" class="form-label">Last Name</label>
                    <input wire:model.defer='input.last_name' type="text" class="form-control" id="last_name" placeholder="Last Name">
                    @error('input.last_name')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>

                <div class="col-md-6">
                    <label for="inputEmail4" class="form-label">Email</label>
                    <input wire:model.defer='input.email' type="email" class="form-control" id="inputEmail4" placeholder="user@example.com">
                    @error('input.email')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-md-6">
                    <label for="phone" class="form-label">Phone</label>
                    <input wire:model.defer='input.phone' type="text" class="form-control" id="phone" placeholder="03XXXXXXXX">
                    @error('input.phone')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>

                <div class="col-md-6">
                    <label for="inputaddress" class="form-label">Address</label>
                    <textarea wire:model.defer='input.address' type="text" class="form-control" id="inputaddress" placeholder="Apartment, studio, or floor"></textarea>
                    @error('input.address')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>

                <div class="col-md-4">
                    <label for="city" class="form-label">City</label>
                    <input wire:model.defer='input.city' type="text" class="form-control" id="city" placeholder="City">
                    @error('input.city')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-md-2">
                    <label for="postal" class="form-label">Postal</label>
                    <input wire:model.defer='input.post_code' type="text" class="form-control" id="postal" placeholder="">
                    @error('input.post_code')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>

                <div class="col-md-6">
                    <label class="" for="inlineFormSelectPref">Service</label>
                    <select wire:model.defer='input.service_id' class="form-select" id="inlineFormSelectPref">
                        <option selected>Choose...</option>
                        @foreach ($services as $service)
                            <option value="{{ $service->id }}">Name: {{ $service->name }} || Price: {{ $service->price }}</option>
                        @endforeach
                    </select>
                    @error('input.service_id')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>

                <div class="col-12">
                    <div class="float-end">
                        <button type="submit" class="btn btn-primary btn-block" wire:loading.attr='disabled'>
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        @endif
