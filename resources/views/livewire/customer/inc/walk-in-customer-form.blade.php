<form wire:submit.prevent='save' class="row g-3 needs-validation" novalidate>

    <div class="col-md-6">
        <label for="first_name" class="form-label">First Name</label>
        <input wire:model.defer='input.first_name' type="text" class="form-control" id="first_name" placeholder="First Name">
        @error('input.first_name')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="col-md-6">
        <label for="last_name" class="form-label">Last Name</label>
        <input wire:model.defer='input.last_name' type="text" class="form-control" id="last_name" placeholder="Last Name">
        @error('input.last_name')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-md-6">
        <label for="inputEmail4" class="form-label">Email</label>
        <input wire:model.defer='input.email' type="email" class="form-control" id="inputEmail4" placeholder="user@example.com">
        @error('input.email')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>
    <div class="col-md-6">
        <label for="phone" class="form-label">Phone</label>
        <input wire:model.defer='input.phone' type="text" class="form-control" id="phone" placeholder="03XXXXXXXX">
        @error('input.phone')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-md-6">
        <label class="" for="inlineFormSelectPref">Service</label>
        <select wire:model.defer='input.service_id' class="form-select" id="inlineFormSelectPref">
            <option selected>Choose...</option>
            @foreach ($services as $service)
                <option value="{{ $service->id }}">Name: {{ $service->name }} || Price: {{ $service->price }}</option>
            @endforeach
        </select>
        @error('input.service_id')
            <span class="text-danger">{{ $message }}</span>
        @enderror
    </div>

    <div class="col-12">
        <div class="float-end">
            <button type="submit" class="btn btn-primary btn-block" wire:loading.attr='disabled'>
                Submit
            </button>
        </div>
    </div>
</form>
