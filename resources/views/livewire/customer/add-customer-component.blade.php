<div>
    <div class="row mb-3">
        <div class="col-8 col-md-6">
            <h3>Add Customer</h3>
        </div>
        <div class="col-4 col-md-6">
            <div wire:loading class="spinner-border text-primary float-end" role="status">
                <span class="visually-hidden">Loading...</span>
            </div>
        </div>
    </div>
    <div class="card p-5">

        <div class="row">
            <div class="col-12 col-md-4 mt-2">
                <div class="d-flex justify-content-md-center">
                    <input wire:click='selectForm(0)' class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" {{ $formType == 0 ? 'checked' : '' }}>
                    <label class="form-check-label" for="gridRadios1">
                        Walk In Customer
                    </label>
                </div>

            </div>
            <div class="col-12 col-md-4 mt-2">
                <div class="d-flex justify-content-md-center">

                    <input wire:click='selectForm(1)' class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" {{ $formType == 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="gridRadios2">
                        Existing customer
                    </label>
                </div>
            </div>
            <div class="col-12  col-md-4 mt-2">
                <div class="d-flex justify-content-md-center">

                    <input wire:click='selectForm(2)' class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" {{ $formType == 2 ? 'checked' : '' }}>
                    <label class="form-check-label" for="gridRadios3">
                        New customer
                    </label>
                </div>
            </div>
        </div>

        <hr class="w-100 mx-auto my-5">

        @if ($formType == 0)
            @include('livewire.customer.inc.walk-in-customer-form')
        @elseif ($formType == 1)
            @include('livewire.customer.inc.exinsting-customer-form')
        @elseif($formType == 2)
            @include('livewire.customer.inc.new-customer-form')
        @endif

    </div>
</div>
