<div>
    <div class="row mb-3">
        <div class="col-12 col-md-6">
            <h3>Customer & Booking</h3>
        </div>
    </div>
    <div class="card p-3">
        <livewire:booking-table />
    </div>

</div>
