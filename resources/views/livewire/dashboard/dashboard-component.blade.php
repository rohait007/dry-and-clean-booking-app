<div>
    <div class="">
        <div class="row">
            <div class="col">
                <div class="page-description">
                    <h1>Dashboard</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 mx-auto">
                <div class="card widget widget-stats">
                    <div class="card-body">
                        <div class="widget-stats-container d-flex">
                            <div class="widget-stats-icon widget-stats-icon-primary">
                                <i class="material-icons-outlined">person</i>
                            </div>
                            <div class="widget-stats-content flex-fill">
                                <span class="widget-stats-title">Total Customers</span>
                                <span class="widget-stats-amount">{{ $data['total_customer'] }}</span>
                                <span class="widget-stats-info">Total Number of Customer </span>
                            </div>
                            {{-- <div class="widget-stats-indicator widget-stats-indicator-negative align-self-start">
                            <i class="material-icons">keyboard_arrow_down</i> 4%
                        </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 mx-auto">
                <div class="card widget widget-stats">
                    <div class="card-body">
                        <div class="widget-stats-container d-flex">
                            <div class="widget-stats-icon widget-stats-icon-danger">
                                <i class="material-icons-outlined">auto_stories</i>
                            </div>
                            <div class="widget-stats-content flex-fill">
                                <span class="widget-stats-title">Total Bookings</span>
                                <span class="widget-stats-amount">{{ $data['total_booking'] }}</span>
                                <span class="widget-stats-info">Total Number of Booking</span>
                            </div>
                            {{-- <div class="widget-stats-indicator widget-stats-indicator-positive align-self-start">
                            <i class="material-icons">keyboard_arrow_up</i> 7%
                        </div> --}}
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>
