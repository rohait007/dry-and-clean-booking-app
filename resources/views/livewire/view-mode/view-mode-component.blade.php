<div class="">
    <li class="nav-link mt-1">
        <a wire:click='switchMode' class="pointer">
            {{-- {{ var_dump($lightMode) }} --}}

            @if ($darkMode)
                <i class="material-icons">light_mode</i>
            @else
                <i class="material-icons">dark_mode</i>
            @endif
        </a>
    </li>
</div>
