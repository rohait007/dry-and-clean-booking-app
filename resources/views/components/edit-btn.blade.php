 <button {{ $attributes->merge(['class' => 'btn btn-sm btn-primary']) }}>
     {{-- <i class="fas fa-edit"></i> --}}
     {{ $slot }}
 </button>
