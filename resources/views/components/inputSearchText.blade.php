<input wire:model.debounce.500ms='searchInput.{{ $model }}' class="form-control rounded-0" placeholder="{{ $placeholder ?? '' }}" type="{{ $type ?? 'text' }}">
