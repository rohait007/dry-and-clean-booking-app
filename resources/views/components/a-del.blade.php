 <a onclick="confirm('Are you sure you want to remove?') || event.stopImmediatePropagation()" {{ $attributes->merge(['class' => 'pointer']) }}>
     {{ $slot }}
 </a>
