<?php

use App\Http\Livewire\Calender\CalanderComponent;
use App\Http\Livewire\Categories\MedicalCategoryComponent;
use App\Http\Livewire\Customer\AddCustomerComponent;
use App\Http\Livewire\Customer\CustomerComponent;
use App\Http\Livewire\Dashboard\DashboardComponent;
use App\Http\Livewire\Departments\DepartmentsComponent;
use App\Http\Livewire\Doctors\Detailcomponent;
use App\Http\Livewire\Doctors\DoctorsComponent;
use App\Http\Livewire\Patients\PatientDetailComponent;
use App\Http\Livewire\Patients\PatientsComponent;
use App\Http\Livewire\Procedure\ProcedureComponent;
use App\Http\Livewire\Rfid\AssignRfidCardComponent;
use App\Http\Livewire\Rfid\RfidCardComponent;
use App\Http\Livewire\Roles\RoleAssignmentComponent;
use App\Http\Livewire\Roles\RoleComponent;
use App\Http\Livewire\Sehat\SehatPatientRecordComponent;
use App\Http\Livewire\Services\ServicesComponent;
use App\Http\Livewire\Staff\StaffComponent;
use App\Http\Livewire\User\ConsultantComponent;
use App\Http\Livewire\User\UserCreateComponent;
use App\Models\Booking;
use App\Models\Sehat\SehatPatientRecord;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function ()
// {
//     // // return view('auth.login');
//     // return redirect()->to('login');
// });


Auth::routes();

Route::group(['middleware' => 'auth'], function ()
{
    Route::get('/', DashboardComponent::class)->name('dashboard');

    Route::group(['prefix' => 'customer'], function ()
    {
        Route::get('/', CustomerComponent::class)->name('customer');
        Route::get('/create', AddCustomerComponent::class)->name('customer.create');
    });

    // Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    // Route::get('/calender', CalanderComponent::class)->name('calender');
    // Route::get('/staff', StaffComponent::class)->name('staff');

    // // Doc Routes
    // Route::get('/doctors', DoctorsComponent::class)->name('doctors');
    // Route::get('/doctor/detail/{user}', Detailcomponent::class)->name('doctors.details');

    // // Rfid
    // Route::get('/rfid/cards', RfidCardComponent::class)->name('rfid.cards');
    // Route::get('/rfid/cards/assignment', AssignRfidCardComponent::class)->name('rfid.assign.card');

    // // patients
    // Route::group(['prefix' => 'patients'], function ()
    // {
    //     Route::get('/', PatientsComponent::class)->name('patients');
    //     Route::get('/create', UserCreateComponent::class)->name('patients.create');
    //     Route::get('/detail', PatientDetailComponent::class)->name('patients.detail');
    // });


    // Route::group(['prefix' => 'procedures'], function ()
    // {
    //     Route::get('/', ProcedureComponent::class)->name('procedures');
    // });

    // Route::group(['prefix' => 'services'], function ()
    // {
    //     Route::get('/', ServicesComponent::class)->name('services');
    // });

    // Route::group(['prefix' => 'department'], function ()
    // {
    //     Route::get('/', DepartmentsComponent::class)->name('department');
    // });
    // Route::group(['prefix' => 'category'], function ()
    // {
    //     Route::get('/', MedicalCategoryComponent::class)->name('category.medical');
    // });
    // Route::group(['prefix' => 'users'], function ()
    // {
    //     Route::get('/consultant', ConsultantComponent::class)->name('user.consultant');
    // });


    // // seyat progeam
    // Route::group(['prefix' => 'sehat'], function ()
    // {
    //     Route::get('/files', SehatPatientRecordComponent::class)->name('sehat.patients.records');
    // });


    // // role
    // Route::group(['prefix' => 'role'], function ()
    // {
    //     Route::get('/', RoleComponent::class)->name('role');
    //     Route::get('/assign', RoleAssignmentComponent::class)->name('role.assign');
    // });


    Route::get('/test', function ()
    {
        dd(Booking::with('customer')->get());
    });
});





Route::get('RunMigration', function ()
{
    Artisan::call('migrate');
    dd('done');
});
