<?php

/**
 * LaraClassified - Classified Ads Web Application
 * Copyright (c) BedigitCom. All Rights Reserved
 *
 * Website: https://bedigit.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

namespace App\Traits;

use App\Models\Customer;

trait CustomerTrait
{
    public function CreateCustomer($data)
    {
        return Customer::create($data);
    }
    public function updateCustomer($data)
    {
        return Customer::where('email', $data['email'])->update($data);
    }
}
