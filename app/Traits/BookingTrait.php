<?php

/**
 * LaraClassified - Classified Ads Web Application
 * Copyright (c) BedigitCom. All Rights Reserved
 *
 * Website: https://bedigit.com
 *
 * LICENSE
 * -------
 * This software is furnished under a license and may be used and copied
 * only in accordance with the terms of such license and with the inclusion
 * of the above copyright notice. If you Purchased from CodeCanyon,
 * Please read the full License from here - http://codecanyon.net/licenses/standard
 */

namespace App\Traits;

use App\Models\Booking;
use Carbon\Carbon;

trait BookingTrait
{
    public function createBooking($service, $customer_id)
    {

        $bookingArr['customer_id'] = $customer_id;
        $bookingArr['service_id'] = $service->id;
        $bookingArr['amount'] = $service->price;
        $bookingArr['booking_date'] = Carbon::now();

        if (Booking::create($bookingArr))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
