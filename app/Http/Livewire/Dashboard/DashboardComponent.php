<?php

namespace App\Http\Livewire\Dashboard;

use App\Models\Booking;
use App\Models\Customer;
use Livewire\Component;

class DashboardComponent extends Component
{
    public function render()
    {
        return view('livewire.dashboard.dashboard-component')
            ->with(
                [
                    'data' => $this->getData()
                ]
            )
            ->extends('layouts.app', ['activePage' => 'dashboard']);
    }

    public function getData()
    {
        $data['total_customer'] = Customer::count();
        $data['total_booking'] = Booking::count();

        return $data;
    }
}
