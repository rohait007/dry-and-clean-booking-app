<?php

namespace App\Http\Livewire\Customer;

use App\Models\Booking;
use App\Models\Customer;
use App\Models\Service;
use App\Models\User;
use App\Traits\BookingTrait;
use App\Traits\CustomerTrait;
use App\Traits\UserTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use Symfony\Component\Console\Input\Input;

class AddCustomerComponent extends Component
{
    use CustomerTrait, BookingTrait, UserTrait;
    public $formType = 0;
    public $input = [];
    public $services;

    public $customers = [], $searchEmail = '', $content = false, $seletedCustomer = null;

    public $password, $confirmPassword, $passwordNotMatch;


    public function mount()
    {
        $this->services = Service::all();
    }

    protected function rules()
    {
        if ($this->formType == 0) // walk in
        {
            return [
                'input.first_name' => 'required',
                'input.last_name' => 'required',
                'input.email' => ['required', 'email', 'unique:customers,email,'],
                'input.phone' => ['required', 'unique:customers,phone,'],
                'input.service_id' => 'required',
            ];
        }
        elseif ($this->formType == 1) // Existing
        {

            return [
                'input.first_name' => 'required',
                'input.last_name' => 'required',
                'input.email' => 'required',
                'input.phone' => 'required',
                'input.service_id' => 'required',
                'input.address' => 'required',
                'input.city' => 'required',
                'input.post_code' => 'required',
            ];
        }
        elseif ($this->formType == 2) // new Customer
        {

            return [
                'input.first_name' => 'required',
                'input.last_name' => 'required',
                'input.email' => ['required', 'email', 'unique:customers,email,', 'unique:users,email,'],
                'password' => 'required',
                'confirmPassword' => 'required',
                'input.phone' => ['required', 'unique:customers,phone,'],
                'input.service_id' => 'required',
                'input.address' => 'required',
                'input.city' => 'required',
                'input.post_code' => 'required',
            ];
        }
    }

    protected $messages = [
        'input.first_name.required' => 'Field Required',
        'input.last_name.required' => 'Field Required',
        'input.email.required' => 'Field Required',
        'input.email.unique' => 'User Already exist againt this Email',
        'input.phone.required' => 'Field Required',
        'input.service.required' => 'Field Required',
        'input.address.required' => 'Field Required',
        'input.city.required' => 'Field Required',
        'input.post_code.required' => 'Field Required',
    ];

    public function render()
    {
        return view('livewire.customer.add-customer-component')
            ->extends('layouts.app', ['activePage' => 'customer']);
    }

    public function selectForm($value)
    {
        $this->formType = $value;
        // $this->resetExcept('formType', 'services');
        $this->resetErrorBag();
        $this->resetValidation();
    }

    public function save()
    {

        $this->validate();
        $service = Service::find($this->input['service_id']);
        if ($this->formType == 0) // walk in
        {
            $this->input['type'] = 'Walk In Customer';
            $this->walkInCustomer($this->input, $service);
        }
        elseif ($this->formType == 1) // Existing
        {
            $this->input['type'] = 'Existing Customer';
            $this->existingCustomer($this->input, $service);
        }
        elseif ($this->formType == 2) // new Customer
        {
            $this->input['type'] = 'New Customer';
            $this->newCustomer($this->input, $service);
        }
    }

    public function walkInCustomer($data, $service)
    {
        $customer =  $this->CreateCustomer($data);

        if ($this->createBooking($service, $customer->id))
        {
            $this->dispatchBrowserEvent('swal:modal', [
                'type' => 'success',
                'title' => 'Success',
                'text' => 'Record Added Successfully',
            ]);

            $this->resetExcept('formType', 'services');
        }
        else
        {
            $this->dispatchBrowserEvent('swal:modal', [
                'type' => 'error',
                'title' => 'Error',
                'text' => 'Something went wrong please try again',
            ]);
        }
    }


    // =============================================================
    //  Existing customer functions
    // =============================================================

    public function updatedSearchEmail()
    {
        $this->content = false;


        if (strlen($this->searchEmail) > 0)
        {
            $this->customers = Customer::where('email', 'like', search($this->searchEmail))->take(2)->get();
        }
        else
        {
            $this->customers = [];
        }
    }
    public function selectUser(Customer $customer)
    {
        $this->content = true;
        $this->input = $customer->toArray();
        $this->searchEmail = $customer->email;
        $this->customers = [];
    }

    public function existingCustomer($data, $service)
    {
        unset($data['service_id']);
        unset($data['created_at']);
        unset($data['updated_at']);

        if ($this->updateCustomer($data))
        {
            if ($this->createBooking($service, $data['id']))
            {
                $this->dispatchBrowserEvent('swal:modal', [
                    'type' => 'success',
                    'title' => 'Success',
                    'text' => 'Record Added Successfully',
                ]);

                $this->resetExcept('formType', 'services');
            }
            else
            {
                $this->dispatchBrowserEvent('swal:modal', [
                    'type' => 'error',
                    'title' => 'Error',
                    'text' => 'Something went wrong please try again',
                ]);
            }
        }
    }



    // =============================================================
    //  new  customer functions
    // =============================================================


    public function updatedConfirmPassword()
    {
        if ($this->password == $this->confirmPassword)
        {
            $this->passwordNotMatch = false;
        }
        else
        {
            $this->passwordNotMatch = true;
        }
    }

    public function newCustomer($data, $service)
    {

        $data['password'] = $this->password;

        $user = $this->createAccount($data);

        $data['user_id'] = $user->id;
        $customer =  $this->CreateCustomer($data);

        if ($this->createBooking($service, $customer->id))
        {
            $this->dispatchBrowserEvent('swal:modal', [
                'type' => 'success',
                'title' => 'Success',
                'text' => 'Record Added Successfully',
            ]);

            $this->resetExcept('formType', 'services');
        }
        else
        {
            $this->dispatchBrowserEvent('swal:modal', [
                'type' => 'error',
                'title' => 'Error',
                'text' => 'Something went wrong please try again',
            ]);
        }
    }
}
