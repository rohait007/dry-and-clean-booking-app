<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;


    protected $fillable = [
        'customer_id',
        'service_id',
        'booking_date',
        'amount'
    ];

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id')->withDefault();
    }
    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id')->withDefault();
    }
}
