<?php

function search($search)
{
    return '%' . $search . '%';
}

function currency()
{
    return 'PKR';
}


function filters($si = [], $q)
{
    foreach ($si as $key => $value)
    {

        $q->where($key, 'like', search($value));
    }
}
